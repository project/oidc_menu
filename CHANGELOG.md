Changelog
=========

All notable changes to the OIDC Menu module.


## 1.0.0-alpha6 (2023-09-14):

### Fixed

* Drupal 10 compatibility.


## 1.0.0-alpha5 (2023-09-01):

### Added

* Support for Drupal 10.


## 1.0.0-alpha4 (2022-05-31):

### Added

* Support for OpenID Connect Client 2.x.

### Removed

* Drupal 8 compatibility.


## 1.0.0-alpha3 (2020-01-26):

### Changed

* Classes on the links wrapper and list.


## 1.0.0-alpha2 (2020-12-01):

### Fixed

* Call to undefined method `count()` when adding a link.
* Links not being rendered.


## 1.0.0-alpha1 (2020-09-22):

First alpha release.
