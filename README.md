OIDC Menu
=========

Provides a user menu block for OpenID Connect users.


## Requirements

* [OpenID Connect Client](https://www.drupal.org/project/oidc)


## Installation

First of all download the latest release and its dependencies using `composer require drupal/oidc_menu`.
You can also download it manually, but please make sure all requirements are fulfilled before continuing.

Next, simply enable the module as described in the [the module installation guide](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

You can add the menu to any page via the block interface at `/admin/structure/block`.
Simply create an `OpenID Connect menu` block and configure it as desired.

Please note that a menu is bound to a specific realm and will only be shown to anonymous
users (if not excluded by role) or users authenticated via that realm. So you might want
to add multiple blocks if you have multiple realms.


## Customization

### Theming

Note that the menu isn't themed out-of-the-box, you have to provide the necessary styling yourself.


### Adding links

You can add your own links to the menu by implementing the `Drupal\oidc_menu\Event\OidcMenuEvents::LINKS` event.

Your subscriber will receive an `Drupal\oidc_menu\Event\MenuLinksEvent` event as argument.
This event allows you to add or alter the previously added link lists (`Drupal\oidc_menu\LinkList`).
