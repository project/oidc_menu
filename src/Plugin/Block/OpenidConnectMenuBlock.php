<?php

namespace Drupal\oidc_menu\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface;
use Drupal\oidc\OpenidConnectSessionInterface;
use Drupal\oidc_menu\Event\MenuLinksEvent;
use Drupal\oidc_menu\Event\OidcMenuEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides the OpenID Connect menu block.
 *
 * @Block(
 *  id = "oidc_menu_block",
 *  admin_label = @Translation("OpenID Connect menu"),
 * )
 */
class OpenidConnectMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The OpenID Connect realm manager.
   *
   * @var \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface
   */
  protected $realmManager;

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\oidc\OpenidConnectSessionInterface
   */
  protected $session;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Class constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManager $realm_manager
   *   The OpenID Connect realm manager.
   * @param \Drupal\oidc\OpenidConnectSessionInterface $session
   *   The OpenID Connect session service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, OpenidConnectRealmManagerInterface $realm_manager, OpenidConnectSessionInterface $session, AccountProxyInterface $current_user, EventDispatcherInterface $event_dispatcher) {
    $this->configFactory = $config_factory;
    $this->realmManager = $realm_manager;
    $this->session = $session;
    $this->currentUser = $current_user;
    $this->eventDispatcher = $event_dispatcher;

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('plugin.manager.openid_connect_realm'),
      $container->get('oidc.openid_connect_session'),
      $container->get('current_user'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = [];

    $config['realm'] = $this->configFactory
      ->get('oidc.provider')
      ->get('default_realm');

    $config['profile_link'] = [
      'enabled' => FALSE,
      'text' => NULL,
      'url' => NULL,
    ];

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $options = [];
    foreach ($this->realmManager->getDefinitions() as $plugin_id => $definition) {
      $options[$plugin_id] = $definition['name'];
    }

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
    ];

    $form['general']['realm'] = [
      '#type' => 'select',
      '#title' => $this->t('Realm'),
      '#options' => $options,
      '#default_value' => $this->configuration['realm'],
      '#required' => TRUE,
    ];

    $form['profile_link'] = [
      '#type' => 'details',
      '#title' => $this->t('User link'),
      '#open' => TRUE,
    ];

    $form['profile_link']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show link'),
      '#default_value' => (int) $this->configuration['profile_link']['enabled'],
    ];

    $form['profile_link']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#description' => $this->t('Leave empty to use the username.'),
      '#default_value' => $this->configuration['profile_link']['text'],
      '#states' => [
        'visible' => [
          ':checkbox[name="settings[profile_link][enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['profile_link']['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Link URL'),
      '#description' => $this->t('Use an external URL, leave empty to link to the user profile.'),
      '#default_value' => $this->configuration['profile_link']['url'],
      '#states' => [
        'visible' => [
          ':checkbox[name="settings[profile_link][enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['realm'] = $form_state->getValue(['general', 'realm']);
    $this->configuration['profile_link'] = [
      'enabled' => FALSE,
    ];

    if ($form_state->getValue(['profile_link', 'enabled'])) {
      $this->configuration['profile_link']['enabled'] = TRUE;

      foreach (['text', 'url'] as $key) {
        $value = trim($form_state->getValue(['profile_link', $key]));

        if ($value !== '') {
          $this->configuration['profile_link'][$key] = $value;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $access = AccessResult::allowedIf($account->isAnonymous());
    $access->addCacheContexts([
      'user.roles:anonymous',
      'user.openid_connect_realm'
    ]);

    if ($this->currentUser->isAuthenticated() && $account->id() === $this->currentUser->id()) {
      $access = $access->orIf(AccessResult::allowedIf(
        $this->session->isAuthenticated() &&
        $this->session->getRealmPluginId() === $this->configuration['realm']
      ));
    }

    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($this->session->isAuthenticated()) {
      return $this->buildAuthenticated();
    }

    return $this->buildAnonymous();
  }

  /**
   * Build the block content for an authenticated user.
   *
   * @return array
   *   A renderable array representing the content of the block.
   */
  protected function buildAuthenticated() {
    $render = [
      '#theme' => 'oidc_menu_authenticated',
      '#title' => $this->label(),
      '#username' => $this->currentUser->getAccount()->getDisplayName(),
      '#logout_link' => Link::createFromRoute($this->t('Log out'), 'oidc.openid_connect.logout'),
    ];

    // Add the profile link.
    $profile_link = $this->configuration['profile_link'];

    if ($profile_link['enabled']) {
      if (isset($profile_link['url'])) {
        $url = Url::fromUri($profile_link['url']);
      }
      else {
        $url = Url::fromRoute('user.page');
      }

      if (isset($profile_link['text'])) {
        $render['#profile_link'] = Link::fromTextAndUrl($profile_link['text'], $url);
      }
      else {
        $render['#username'] = Link::fromTextAndUrl($render['#username'], $url);
      }
    }

    // Add the links.
    /** @var \Drupal\oidc_menu\Event\MenuLinksEvent $menu_links_event */
    $menu_links_event = $this->eventDispatcher->dispatch(
      new MenuLinksEvent($this),
      OidcMenuEvents::LINKS
    );

    if ($menu_links_event->countLists(FALSE)) {
      $links = [];
      foreach ($menu_links_event->getLists(FALSE) as $name => $link_list) {
        $items = [];

        /** @var \Drupal\Core\Link $link */
        foreach ($link_list as $link) {
          $items[] = [
            'value' => $link->toRenderable(),
          ];
        }

        $links[$name] = [
          '#theme' => 'item_list__oidc_menu',
          '#title' => $link_list->getTitle(),
          '#items' => $items,
          '#list_type' => 'ul',
          '#attributes' => [
            'class' => ['links'],
          ],
          '#wrapper_attributes' => [
            'class' => ['links-list', Html::getClass('links-list--' . $name)],
          ],
        ];
      }

      $render['#links'] = $links;
    }

    return $render;
  }

  /**
   * Build the block content for an anonymous user.
   *
   * @return array
   *   A renderable array representing the content of the block.
   */
  protected function buildAnonymous() {
    return [
      '#theme' => 'oidc_menu_anonymous',
      '#login_link' => Link::createFromRoute($this->t('Log in'), 'oidc.openid_connect.login', [
        'realm' => $this->configuration['realm'],
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'user.openid_connect';

    if ($this->session->isAuthenticated()) {
      $contexts[] = 'user';
    }

    return $contexts;
  }

}
