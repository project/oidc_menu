<?php

namespace Drupal\oidc_menu;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Link;

/**
 * Represents a list of links to be shown in the menu.
 */
class LinkList implements \Iterator {

  /**
   * The list title.
   *
   * @var string
   */
  protected string $title;

  /**
   * The links in this list.
   *
   * Each entry is an associative array with a 'link' and 'weight' element.
   *
   * @var array[]
   */
  protected array $links = [];

  /**
   * Whether the links are sorted by weight.
   *
   * @var bool
   */
  protected bool $sorted = TRUE;

  /**
   * Default weight for the next link.
   *
   * @var int
   */
  protected int $nextWeight = 0;

  /**
   * Class constructor.
   *
   * @param string|null $title
   *   The list title.
   */
  public function __construct(?string $title = NULL) {
    $this->setTitle($title);
  }

  /**
   * Get the list title.
   */
  public function getTitle(): ?string {
    return $this->title ?? NULL;
  }

  /**
   * Set the list title.
   *
   * @param string|null $title
   *   The new title.
   *
   * @return $this
   */
  public function setTitle(?string $title = NULL): self {
    if ($title !== NULL && $title !== '') {
      $this->title = $title;
    }
    else {
      unset($this->title);
    }

    return $this;
  }

  /**
   * Get all links.
   *
   * @return Link[]
   *   All links in the list, sorted by weight.
   */
  public function getAll(): array {
    $this->sort();

    return array_map(fn ($item) => $item['link'], $this->links);
  }

  /**
   * Get a link.
   *
   * @param int $index
   *   The index of the link.
   *
   * @return Link|null
   *   The link instance or NULL if the link doesn't exist.
   */
  public function get(int $index): ?Link {
    return $this->links[$index]['link'] ?? NULL;
  }

  /**
   * Add a link.
   *
   * @param Link $link
   *   The link to add.
   * @param int|null $weight
   *   The link weight.
   *
   * @return $this
   */
  public function add(Link $link, ?int $weight = NULL): self {
    $this->links[] = [
      'link' => $link,
      'weight' => $weight ?? $this->nextWeight++,
    ];

    $this->sorted = $this->count() <= 1;

    return $this;
  }

  /**
   * Remove a link by index.
   *
   * @param int $index
   *   The index of the link.
   *
   * @return $this
   */
  public function remove(int $index): self {
    unset($this->links[$index]);

    $this->sorted = $this->count() === 1;

    return $this;
  }

  /**
   * Remove a link by instance.
   *
   * @param Link $link
   *   The link object.
   * @param bool $multiple
   *   Set to TRUE to remove all instances (instead of only the first one).
   *
   * @return $this
   */
  public function removeByInstance(Link $link, bool $multiple = FALSE): self {
    foreach ($this->links as $index => $item) {
      if ($item['link'] === $link) {
        $this->remove($index);
        if (!$multiple) {
          break;
        }
      }
    }

    return $this;
  }

  /**
   * Get the number of links.
   */
  public function count(): int {
    return count($this->links);
  }

  /**
   * Sort the links.
   */
  protected function sort(): void {
    if (!$this->sorted) {
      uasort($this->links, [SortArray::class, 'sortByWeightElement']);
      $this->sorted = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function current(): ?Link {
    $this->sort();
    $item = current($this->links);

    return $item ? $item['link'] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function next(): void {
    $this->sort();

    next($this->links);
  }

  /**
   * {@inheritdoc}
   */
  public function key(): ?int {
    $this->sort();

    return key($this->links);
  }

  /**
   * {@inheritdoc}
   */
  public function valid(): bool {
    $this->sort();

    return key($this->links) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function rewind(): void {
    $this->sort();

    reset($this->links);
  }

}
