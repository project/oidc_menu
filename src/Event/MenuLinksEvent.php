<?php

namespace Drupal\oidc_menu\Event;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\oidc_menu\LinkList;
use Drupal\oidc_menu\Plugin\Block\OpenidConnectMenuBlock;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event to allow other modules to add links to the menu.
 */
class MenuLinksEvent extends Event {

  /**
   * The OpenID Connect menu block.
   *
   * @var \Drupal\oidc_menu\Plugin\Block\OpenidConnectMenuBlock
   */
  protected $block;

  /**
   * The link lists.
   *
   * Each entry is an associative array with a 'list' and 'weight' element.
   *
   * @var array
   */
  protected $lists = [];

  /**
   * Wether the lists are sorted by weight.
   *
   * @var bool
   */
  protected $sorted = TRUE;

  /**
   * Default weight for the next list.
   *
   * @var int
   */
  protected $nextWeight = 0;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc_menu\Plugin\Block\OpenidConnectMenuBlock $block
   *   The OpenID Connect menu block.
   */
  public function __construct(OpenidConnectMenuBlock $block) {
    $this->block = $block;
  }

  /**
   * Get the block plugin ID.
   *
   * @return string
   */
  public function getBlockId() {
    return $this->block->getPluginId();
  }

  /**
   * Get the block configuration.
   *
   * @return array
   */
  public function getBlockConfiguration() {
    return $this->block->getConfiguration();
  }

  /**
   * Get the realm plugin ID.
   *
   * @return string
   */
  public function getRealmId() {
    return $this->block->getConfiguration()['realm'];
  }

  /**
   * Get all link lists.
   *
   * @param bool $include_empty
   *   Set to FALSE to exclude empty lists.
   *
   * @return \Drupal\oidc_menu\LinkList[]
   *   All link lists, sorted by weight.
   */
  public function getLists($include_empty = TRUE) {
    $this->sortLists();

    $lists = [];
    foreach ($this->lists as $index => $item) {
      if ($include_empty || $item['list']->count()) {
        $lists[$index] = $item['list'];
      }
    }

    return $lists;
  }

  /**
   * Get a link list.
   *
   * @param string $name
   *   The name to identify the list.
   *
   * @return \Drupal\oidc_menu\LinkList|NULL
   *   The list or NULL if the list doesn't exist.
   */
  public function getList($name) {
    if (isset($this->lists[$name])) {
      return $this->lists[$name]['list'];
    }
  }

  /**
   * Add a link list.
   *
   * @param string $name
   *   A unique name (e.g. the module name) to identify the list.
   * @param \Drupal\oidc_menu\LinkList $list
   *   The list to add.
   * @param int|null $weight
   *   The list weight.
   *
   * @return $this
   *
   * @throws \InvalidArgumentException
   */
  public function addList($name, LinkList $list, $weight = NULL) {
    $name = (string) $name;

    if (isset($this->lists[$name])) {
      throw new \InvalidArgumentException("Link list '$name' was already added.");
    }

    if ($weight === NULL) {
      $weight = $this->nextWeight;
      $this->nextWeight++;
    }

    $this->lists[$name] = [
      'list' => $list,
      'weight' => $weight,
    ];

    if ($this->countLists() > 1) {
      $this->sorted = FALSE;
    }

    return $this;
  }

  /**
   * Remove a link list.
   *
   * @param string $name
   *   The name to identify the list.
   *
   * @return $this
   */
  public function removeList($name) {
    $name = (string) $name;

    if (isset($this->lists[$name])) {
      unset($this->lists[$name]);

      if ($this->countLists() === 1) {
        $this->sorted = TRUE;
      }
    }

    return $this;
  }

  /**
   * Remove a link list.
   *
   * @param \Drupal\oidc_menu\LinkList $list
   *   The list object.
   * @param bool $multiple
   *   Set to TRUE to remove all instances (instead of only the first one).
   *
   * @return $this
   */
  public function removeListByInstance(LinkList $list, $multiple = FALSE) {
    foreach ($this->lists as $index => $item) {
      if ($item['list'] === $list) {
        $this->removeList($index);

        if (!$multiple) {
          break;
        }
      }
    }

    return $this;
  }

  /**
   * Get the number of link lists.
   *
   * @param bool $include_empty
   *   Set to FALSE to exclude empty lists.
   *
   * @return int
   */
  public function countLists($include_empty = TRUE) {
    if ($include_empty) {
      return count($this->lists);
    }

    $count = 0;
    foreach ($this->lists as $item) {
      if ($item['list']->count()) {
        $count++;
      }
    }

    return $count;
  }

  /**
   * Sort the link lists.
   */
  protected function sortLists() {
    if (!$this->sorted) {
      uasort($this->lists, [SortArray::class, 'sortByWeightElement']);

      $this->sorted = TRUE;
    }
  }

}
