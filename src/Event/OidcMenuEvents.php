<?php

namespace Drupal\oidc_menu\Event;

/**
 * Defines the events of the OpenID Connect Menu module.
 */
final class OidcMenuEvents {

  /**
   * Name of the event fired when collecting the menu links.
   *
   * This event allows modules to provide their own links for the menu.
   * The event listener method receives an instance of
   * \Drupal\oidc_menu\Event\MenuLinksEvent.
   *
   * @Event
   *
   * @see \Drupal\oidc_menu\Event\MenuLinksEvent
   *
   * @var string
   */
  const LINKS = 'oidc_menu.links';

}
